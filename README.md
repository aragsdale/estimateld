# README #

This repository accompanies the manuscript "Unbiased estimation of linkage disequilibrium from unphased data."

Here, you'll find the manuscript and code to replicate its results. There are also some 
Jupyter notebooks to explore one- and two-locus statistics in the Thousand Genomes Project data
and to play around with moments.LD (found at bitbucket.org/simongravel/moments) 
to get two-locus statistics and run demographic inference.

### What's in here? ###

* The manuscript and figures (including data and scripts to recreate figures) can be found in manuscript
* Data to run analyses in tutorial can be found in data
* Mathematica notebook to compute unbiased estimators for 2-locus statistics is in derivation
* Jupyter notebooks to play around with some data are in tutorial

### How do I get set up? ###

* The tutorials work best in python 3 (I've tested in python 3.6, and sci-kit.allel might not work properly in python 2.7)
* To run the data exploration tutorial, we use sci-kit.allel (version 1.2 or higher) and hdf5
* To run the demographic inference, we use moments.LD

### Installing moments and moments.LD ###

### Any questions or comments? ###

Aaron Ragsdale : aaron.ragsdale@mail.mcgill.ca or @apragsdale on Twitter
