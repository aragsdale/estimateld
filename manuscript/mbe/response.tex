\documentclass[onecolumn]{article}
\usepackage{fullpage}

\usepackage{authblk}
\usepackage{float, graphicx}
\usepackage[margin=2.54cm]{geometry}
%\usepackage[margin=3.5cm]{geometry}
\usepackage{amsmath,amsfonts,amssymb}
\usepackage{wrapfig}
\usepackage{color}
\usepackage{pdflscape}
\usepackage{multicol}
\usepackage[final]{pdfpages}
\usepackage{multirow}
\usepackage{url}
\usepackage{mathtools}
\usepackage{enumitem}
\usepackage{bm}

\setcounter{MaxMatrixCols}{30}

\usepackage{makecell}

\usepackage{placeins}
%\usepackage[round, numbers]{natbib}
\usepackage[round]{natbib}
\renewcommand{\cite}{\citep}
\setlength{\bibhang}{0pt}

\usepackage{totcount}
\newtotcounter{citnum} %From the package documentation
\def\oldbibitem{} \let\oldbibitem=\bibitem
\def\bibitem{\stepcounter{citnum}\oldbibitem}

\usepackage{color}
\newcommand{\comment}[1]{\textcolor{blue}{APR: #1}}
\newcommand{\sgcomment}[1]{\textcolor{red}{SG: #1}}

\newcommand{\E}{\mathbb{E}}
\newcommand{\F}{\emph{F}}

\DeclareMathOperator*{\sumsum}{\sum\sum}

\allowdisplaybreaks

\usepackage[displaymath,mathlines]{lineno}
%\linenumbers

\linespread{1.}

\usepackage{parskip}
\setlength{\parskip}{5pt} % 1ex plus 0.5ex minus 0.2ex}
\setlength{\parindent}{0pt}

\usepackage[dvipsnames]{xcolor}

\begin{document}

\textbf{Response to reviewers} for\\
``Unbiased estimation of linkage disequilibrium from unphased data''

\begin{quotation}
We would like to thank the reviewers for their thoughtful and attentive comments on our manuscript.
We're happy to have the opportunity to improve our manuscript by addressing their concerns and suggestions.
Below we address each of the comments in turn, and our responses are inset.

Sincerely,\\
Aaron Ragsdale and Simon Gravel
\end{quotation}

Editors' comments to the author:

The comments and recommendations from two expert reviewers are now available for your manuscript.
These reviewers judged the reported discoveries to be of high significance, and the potential scientific impact of your work to be high.
They found that the manuscript needs additional analysis to compare the proposed estimator with those by Excoffier \& Slatkin and probably also by Song and Song.
Editors generally agree with their concerns and recommendations, which led to a designation of high priority.
The recommendation is given assuming that you will be able to satisfactorily address all the reviewers' comments in revision.
In addition, it was not clear to me what is the difference between $r^2_\div$ defined in line 87 and $\sigma^2_D$ defined in the second-to-last line of Figure 2 legend.

\begin{quotation}
Thank you for the opportunity to address the reviewers' concerns.
Below, we respond to each suggestion in turn and point to the relevant section in the text that was changed.
We restructured the introduction and discussion to better frame our results.
The discussion is now split into subsections (Challenges of computing $r^2$ and Tradeoffs and limitations) to include suggested discussion and help with readability.
We also included additional analyses comparing our estimator for $r^2$ to that of Excoffier and Slatkin and to compare its speed and computational complexity. We have also added discussion of the Song and Song method, as outlined below.

As you point out, our previous definitions of $r^2_\div$ and $\sigma_D^2$ and their difference was confusing.
We now more carefully define these estimators ($r^2_\div$ is computed for each pair of loci and then averaged, where $\sigma_D^2$ takes the averages of $D^2$ and $\pi_2$ first, and then takes their ratio).
The subsection Sources of LD in the Introduction should now help to orient readers to these differences, and this section also discusses the differences between population-level and finite sample LD, as suggested by reviewer 1.
\end{quotation}

Reviewer \#1:

In this manuscript, Ragsdale and Gravel discuss some subtle aspects of widely-used measures of linkage disequilibrium (LD).  In particular, $r^2$ is defined at the population level as the squared correlation of alleles at two loci.  Standard and widely used plug-in estimates for  $r^2$ are substantially biased for small sample sizes.  The same holds true for a number of other measures of LD.  The authors then present what is essentially a finite sample correction to these measures to produce either unbiased or less biased estimators of these various measures of LD.  While methodologically such corrections are standard in statistics (e.g. Bessel's correction is taught in introductory statistics courses), the finite sample bias is not usually discussed in this particular aspect of population genetics and its correction is an important advance.  Overall, the manuscript was exceptionally well-written, clear, and should be useful to a wide range of practitioners that use measures of LD in their work.  I have no major comments, but have a few very minor points that I raise below.

I prefer to sign my reviews,\\
Jeffrey P. Spence

Minor Points:

Line 23:  I think it would improve the exposition to introduce very early the difference between finite-sample and population-level LD.  This difference is key to understanding the source of the bias.  It would also be good to point out the connection to Bessel's correction, and to mention the tension in that context of the bias-variance tradeoff (e.g., for univariate Gaussian observations, using $n-1$ in the denominator of an estimator of variance is unbiased, using $n$ is the MLE, and using $n+1$ is the minimum MSE estimator).

\begin{quotation}
We agree that this improves the exposition.
We now include a subsection at the end of the introduction that discusses the difference between population-level and finite-sample LD, and the motivation for estimating population-level LD.
This allows for a natural motivation for considering $\sigma_d^2$.
We also expand the Discussion to point out the connection to Bessel's correction and the bias-variance tradeoff.
\end{quotation}

Figure 1E: include a color scale.

\begin{quotation}
Thank you for catching this oversight -- the colorbar is now included.
\end{quotation}

Line 244: It would be good to define $g_1,...$ in terms of the actual genotypes at each locus.  

\begin{quotation}
We use $g_1, ...$ and $n_1, ...$ as shorthand for $g_{AABB},...$ and $n_{AABB},...$, because the latter takes up a lot of room when writing out expressions, needing multiple lines and making them difficult to read.
We now more clearly define the $g_i$ when introducing the notation (before, as the reviewer points out, the manuscript required the reader to infer their definition from Table 2).
Table 2 caption and the Notation subsection in Methods now explicitly describes this notation.
\end{quotation}

The reason that it is difficult to obtain unbiased estimates for $r^2$ is that it is a rational function of haplotype frequencies (as opposed to a polynomial).  The results presented in the present manuscript allow unbiased estimation of arbitrary polynomials of such frequencies, but not these rational functions.  Song and Song (TPB, 2007) present a series expansion of $r^2$ in terms of such polynomials, and so by truncating this expansion and using unbiased estimators for each of the terms, an approximately unbiased estimator (up to the effect of the truncation) for $r^2$ could be obtained.  Implementing this approach is certainly not necessary for the present manuscript, but since $r^2$ is much more widely used than $\sigma_D^2$, such an estimator would be of interest to the community.

\begin{quotation}
This is a good point, which we hadn't considered.
In the Discussion, we now discuss this as a possible approach.
One potential issue is that the expansion may require many terms to provide accurate estimates, which could be computationally burdensome.
\end{quotation}

Reviewer \#2:

This excellent manuscript solves an important problem, which has impeded our ability to interpret linkage disequilibrium (LD) statistics. We have long had an unbiased estimator of the Lewontin-Kojima parameter, $D$, which measures LD. In data analysis, we typically square estimates ($\hat{D}^2$) of this parameter, because we are not interested in the sign. However, $\hat{D}^2$ is not an unbiased estimate of $D^2$. Similar remarks apply to $r$ and $r^2$, the most common normalized parameters describing LD. These problems may lead to biased estimates of population size and spurious indications of admixture. In two recent manuscripts (this one and another that I recently reviewed), Ragsdale and Gravel have established a new framework for estimating and interpreting LD statistics, which largely solves these problems. I suspect that the entire field will soon be using this new framework.

I have two suggestions. First, I would compare the new method not only against $r^2_{RH}$, but also against the EM estimator of Excoffier and Slatkin. The EM estimator has a smaller sampling variance than $r^2_{RH}$ , and this implies that its square would be less biased than $r^2_{RH}$.

\begin{quotation}
We now compare our approach to both the RH and EM approaches in the main text and supplement (Figure 1 and Figures S1 and S3 now include the Excoffier and Slatkin results).
As you suggest, the EM approach often has smaller sampling variance than the RH approach, leading to less biased $r^2$.
However, every estimator shows some bias, and the degree of bias varies with different underlying allele frequencies and LD (see Fig S3).
The same can be said of the standard errors for each estimator, and we include the EM approach in the comparison of SEs in figure S3.
\end{quotation}

Second, it would be useful to compare the speeds of the various estimators. Rogers and Huff argued that their estimator was useful in spite of its larger bias, because of its greater speed. As readers evaluate the potential utility of the Ragsdale-Gravel estimator, they will want to know about its speed.

\begin{quotation}
We agree that it would be useful to readers to have an idea of the computational speed and complexity of our estimator.
We include a comparison of the speed of our implementation of our estimators to that of the Rogers-Huff approach, computed using the correlation coefficients from the genotype matrix.
The RH approach is much faster, as expected, so there is the same trade-off they found between speed of computation and bias.
As seen in figure S7 in the Supplement, it is clear that our Python implementation is much slower that the RH estimator, although the complexity is such that we can still compute our estimators for sample sizes in the tens of thousands of diploid samples in reasonable time.
Furthermore, while the RH approach has been heavily optimized, our approach has been written largely in Python, and we expect significant gains to be made by focusing on algorithmic efficiency.

These trade-offs are now included in the Discussion.
\end{quotation}

Detailed comments are keyed to line numbers. There are two sets of line numbers in the manuscript. I'm using the set closer to the text and farther from the left margin. This set numbers lines sequentially throughout the manuscript.

50: It's not clear what ``2.1'' refers to.

\begin{quotation}
``2.1'' refers to the estimated $N_e$ from Funk et al (2016), and we've edited that sentence to now make that more clear.
\end{quotation}

59-61: I would cite sources for these formulae, even though they are classics that will be familiar to most readers.

\begin{quotation}
We now include references to Lewontin and Kojima (1960) and Hill and Robertson (1968) for these formulae.
\end{quotation}

67-69: When referring to $E[r^2]$ and $E[D^2]$, it's important to say whether monomorphic sites are included. Yun Song's expected value includes such sites, assuming that $r = 0$ when there is no variation. (There is apparently a well-defined limit as variation approaches zero.) But in data analysis, monomorphic sites are usually excluded. So Song's algorithm doesn't calculate the expected value of the thing we actually estimate. I suggest that the authors clarify this here.

\begin{quotation}
We agree that this is an important distinction.
In the Introduction, we now include a subsection titled ``Sources of LD'' that clarifies which statistics are conditional on polymorphism and which are not.
An advantage of using $\sigma_D^2$ when comparing model predictions to data is that the inclusion or exclusion of monomorphic sites does not change either the expected or observed statistic.
As you point out, the values of $r^2$ reported in Figure 2 are computed using only polymorphic loci, making it difficult to compare to the expectation from Song and Song.
\end{quotation}

92-93: I'm not sure what the authors mean by ``converged to the true $r^2$ faster.'' Are the authors referring to convergence as sample size increases, or as one adds terms to some series expansion, or what?

\begin{quotation}
Yes, this was unclear.
We were referring to convergence to the population-level $r^2$ as sample size increases.
\end{quotation}

Fig 2 caption: How does one incorporate phase into $r^2_{RH}$? I thought this estimator was designed for use only with unphased data.

\begin{quotation}
The term ``phased RH estimator'' is a bit of a misnomer.
We mean the approach of estimating $r$ by computing the correlation between two genotype vectors of 0s and 1s (if we know phasing) and squaring that result (instead of the Rogers-Huff method that takes unphased genotype vectors with entries of 0, 1, and 2).
Because the variance of estimating $r$ with phased data is less than the variance using unphased data, this results in differences in $r^2$ estimates between the RH approach on unphased data and the analogous approach on phased data.
Instead of calling it $r^2_{RH}$ phased or unphased, we now more generally call this approach $\hat{r}^2$ phased or unphased, and clarify that ``$\hat{r}^2$ unphased'' is $r^2_{RH}$.
\end{quotation}

Fig 3 caption: I'm not sure what the authors mean by ``the estimation due to Ohta and Kimura.'' Ohta and Kimura defined $\sigma_D^2$ as a parameter but did not try to estimate it.

\begin{quotation}
We agree that this language was confusing.
What we meant by ``the estimation due to Ohta and Kimura'' is their diffusion approximation-based formula for the expectation of $\sigma_D^2$ under equilibrium conditions.
We have changed the language in the figure 3 captions and text to make it more clear that this is an approximation for the expected $\sigma_D^2$ under demographic equilibrium rather than an estimator.
\end{quotation}


\end{document}

