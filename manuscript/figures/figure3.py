import matplotlib.pylab as plt
import matplotlib
import numpy as np
import pickle
import pickle, numpy as np
import matplotlib.pylab as plt
from matplotlib.ticker import FuncFormatter


# Set fontsize to 10
matplotlib.rc('font',**{#'family':'sans-serif',
                        #'sans-serif':['Helvetica'],
                        'style':'normal',
                        'size':8 })
# Set label tick sizes to 8
matplotlib.rc('xtick', labelsize=6)
matplotlib.rc('ytick', labelsize=6)

def Ne_estimate(r, S, u):
    return (0.5*(r*(2. - 26.*S) + 16.*u - 128.*S*u + np.sqrt(r**2*(4. + 56.*S + 324.*S**2) + r*(64. + 576.*S + 2432.*S**2)*u + (256. + 1024.*S + 5120.*S**2)*u**2)))/(S*(8.*r**2 + 96.*r*u + 256.*u**2))


r = 2e-8
u = 2e-8

bins = np.linspace(0,5e-3,21)
bin_mids = (bins[:-1]+bins[1:])/2

data = pickle.load(open('data/simulated_s2s.bp','rb'))

msprime_s2 = data['n10000']
msprime_s2_500 = data['n500']

Ne = 1e4
#WH_d2 = (bins**2 + (1-bins)**2) / (2*Ne*bins * (2-bins))
OK_s2 = 1./(3 + 4*Ne*(bins+4*u) - (2./(2.5+Ne*(bins+4*u)+Ne*4*u)))
OK_s2_mid = 1./(3 + 4*Ne*(bin_mids+4*u) - (2./(2.5+Ne*(bin_mids+4*u)+Ne*4*u)))

OK = 1./6 * (4*OK_s2_mid + OK_s2[:-1] + OK_s2[1:])

Ne_left = Ne_estimate(bins[:-1], msprime_s2, u)
Ne_mid = Ne_estimate(bin_mids, msprime_s2, u)
Ne_right = Ne_estimate(bins[1:], msprime_s2, u)

Nes = (Ne_left+Ne_right+4*Ne_mid)/6


Ne = 500
#WH_d2 = (bins**2 + (1-bins)**2) / (2*Ne*bins * (2-bins))
OK_s2_500 = 1./(3 + 4*Ne*(bins+4*u) - (2./(2.5+Ne*(bins+4*u)+Ne*4*u)))
OK_s2_mid_500 = 1./(3 + 4*Ne*(bin_mids+4*u) - (2./(2.5+Ne*(bin_mids+4*u)+Ne*4*u)))

OK_500 = 1./6 * (4*OK_s2_mid_500 + OK_s2_500[:-1] + OK_s2_500[1:])

Ne_left_500 = Ne_estimate(bins[:-1], msprime_s2_500, u)
Ne_mid_500 = Ne_estimate(bin_mids, msprime_s2_500, u)
Ne_right_500 = Ne_estimate(bins[1:], msprime_s2_500, u)

Nes_500 = (Ne_left_500+Ne_right_500+4*Ne_mid_500)/6

fig = plt.figure(3, figsize=(6.5, 2.5))
fig.clf()

ax1 = plt.subplot(1,2,1)


ax1.plot( 100 * bin_mids, msprime_s2_500, '^', linewidth=1, color='black', markersize=3, label='$N_e=500$, $n=10$')
ax1.plot( 100 * bin_mids, msprime_s2, 'v', linewidth=1, color='black', markersize=3, label='$N_e=10,000$, $n=50$')
ax1.plot( 100 * bin_mids, OK, '--', color='black', linewidth=1, label=None)
ax1.plot( 100 * bin_mids, OK_500, '--', color='black', linewidth=1, label='Ohta and Kimura')

ax1.set_yscale('log')
ax1.set_xscale('log')

ax1.set_xlabel('cM')
ax1.set_ylabel('$\sigma_D^2$')

ax1.legend(frameon=False, fontsize=6)

ax1.set_xticks([0.01,0.1,0.5])
ax1.set_xticklabels([0.01,0.1,0.5])

ax2 = plt.subplot(1,2,2)

ax2.plot(100*bin_mids[1:], ((Ne_left_500+Ne_right_500+4*Ne_mid_500)/6)[1:] / 500, '^--', linewidth=1, color='black', markersize=3, label='$N_e=500$, $n=10$')
ax2.plot(100*bin_mids[1:], ((Ne_left+Ne_right+4*Ne_mid)/6)[1:] / 10000, 'v--', linewidth=1, color='black', markersize=3, label='$N_e=10,000$, $n=50$')

ax2.legend(frameon=False, fontsize=6)

ax2.set_ylim([0.85,1.15])

xlims = ax2.get_xlim()
ax2.plot(ax2.get_xlim(), [1,1], 'k-', linewidth=.5, label=None)
ax2.set_xlim(xlims)

ax2.set_xlabel('cM')
ax2.set_ylabel('$\widehat{N}_e / N_e$')

fig.tight_layout()

fig.text(0.025,0.95,'A', va='center', ha='center', fontsize=9)
fig.text(0.515,0.95,'B', va='center', ha='center', fontsize=9)

plt.savefig('figure3.pdf')
