import numpy as np
import scipy.special
from moments.LD import stats_from_genotype_counts as sgc
from moments.LD import stats_from_haplotype_counts as shc
import pickle

## cache D^2 estimates for varying sample size

def ragsdale_D2(Gs):
    D2 = sgc.DD([Gs],[0,0])
    return D2

def cov_D2(Gs):
    D2 = np.zeros(len(Gs[0]))
    for i in range(len(Gs[0])):
        gl = [2]*Gs[0,i] + [2]*Gs[1,i] + [2]*Gs[2,i] + [1]*Gs[3,i] + [1]*Gs[4,i] + [1]*Gs[5,i] +  [0]*Gs[6,i] + [0]*Gs[7,i] + [0]*Gs[8,i]
        gr = [2]*Gs[0,i] + [1]*Gs[1,i] + [0]*Gs[2,i] + [2]*Gs[3,i] + [1]*Gs[4,i] + [0]*Gs[5,i] +  [2]*Gs[6,i] + [1]*Gs[7,i] + [0]*Gs[8,i]
        #rh_rs[i] = np.corrcoef(gl,gr)[0,1]
        D2[i] = (1./2*np.cov(gl,gr))[0,1]**2
    return D2


freqs = [ ( 0.5, 0.5, 0.0 ),
          ( 0.4, 0.6, 0.1 ),
          ( 0.1, 0.2, 0.05 ),
          ( 0.5, 0.05, -0.02 )] 

data = {}
s = 100000
ns = [10,20,40,100,200,400]

for p,q,D in freqs:
    print(p,q,D)
    fAB = p*q+D
    fAb = p*(1-q)-D
    faB = (1-p)*q-D
    fab = (1-p)*(1-q)+D


    (g1,g2,g3,g4,g5,g6,g7,g8,g9) = (fAB**2, 2*fAB*fAb, fAb**2, 2*fAB*faB, 2*fAB*fab+2*fAb*faB, 2*fAb*fab, faB**2, 2*faB*fab, fab**2)


    mean_D2s = {}
    mean_D2s['AR'] = np.zeros(len(ns))
    mean_D2s['Cov'] = np.zeros(len(ns))

    for ii,n in enumerate(ns):
        Gs = np.random.multinomial(n, (g1,g2,g3,g4,g5,g6,g7,g8,g9), size=s).transpose()
        D2_AR = ragsdale_D2(Gs)
        mean_D2s['AR'][ii] = np.mean(D2_AR)
        
        D2_cov = cov_D2(Gs)
        mean_D2s['Cov'][ii] = np.mean(D2_cov)
    
    data[(p,q,D)] = {'mean': mean_D2s}


pickle.dump(data, open('mean_D2.bp', 'wb+'))

## cache r^2 estimates for varying sample size

def ragsdale_r2(Gs):
    D2 = sgc.DD([Gs],[0,0])
    pi2 = sgc.pi2([Gs], [0,0,0,0])
    return D2/pi2

def rogers_r2(Gs):
    rh_rs = np.zeros(len(Gs[0]))
    for i in range(len(Gs[0])):
        gl = [2]*Gs[0,i] + [2]*Gs[1,i] + [2]*Gs[2,i] + [1]*Gs[3,i] + [1]*Gs[4,i] + [1]*Gs[5,i] +  [0]*Gs[6,i] + [0]*Gs[7,i] + [0]*Gs[8,i]
        gr = [2]*Gs[0,i] + [1]*Gs[1,i] + [0]*Gs[2,i] + [2]*Gs[3,i] + [1]*Gs[4,i] + [0]*Gs[5,i] +  [2]*Gs[6,i] + [1]*Gs[7,i] + [0]*Gs[8,i]
        rh_rs[i] = np.corrcoef(gl,gr)[0,1]
    return rh_rs

def burrows_r2(Gs, n):
    DD = sgc.Dhat(Gs)**2
    h1 = 1.*(Gs[0] + Gs[1] + Gs[2]) / n
    h2 = 1.*(Gs[0] + Gs[3] + Gs[6]) / n
    p = 1.*(Gs[0] + Gs[1] + Gs[2] + 1./2*Gs[3] + 1./2*Gs[4] + 1./2*Gs[5]) / n
    q = 1.*(Gs[0] + Gs[3] + Gs[6] + 1./2*Gs[1] + 1./2*Gs[4] + 1./2*Gs[7]) / n
    denom = (p*(1-p) + (h1-p**2)) * (q*(1-q) + (h2-q**2))
    return DD/denom


def bulik_sullivan(Gs, n):
    bs_rs = np.zeros(len(Gs[0]))
    for i in range(len(Gs[0])):
        gl = [2]*Gs[0,i] + [2]*Gs[1,i] + [2]*Gs[2,i] + [1]*Gs[3,i] + [1]*Gs[4,i] + [1]*Gs[5,i] +  [0]*Gs[6,i] + [0]*Gs[7,i] + [0]*Gs[8,i]
        gr = [2]*Gs[0,i] + [1]*Gs[1,i] + [0]*Gs[2,i] + [2]*Gs[3,i] + [1]*Gs[4,i] + [0]*Gs[5,i] +  [2]*Gs[6,i] + [1]*Gs[7,i] + [0]*Gs[8,i]
        #rh_rs[i] = np.corrcoef(gl,gr)[0,1]
        pA = 1.*sum(gl)/2/len(gl)
        pB = 1.*sum(gr)/2/len(gl)
        r = np.corrcoef(gl,gr)[0,1]
        bs_rs[i] = r**2 - (1-r**2)/(n-2.)
    return bs_rs


## EM algorithm, using Hill 1974
## get ML estimate for fAB

tol = 1e-7

def hill_rs(Gs, n):
    em_r2 = np.zeros(len(Gs[0]))
    for i in range(len(Gs[0])):
        N11,N12,N13,N21,N22,N23,N31,N32,N33 = Gs[:,i]
        X11 = 2*N11 + N12 + N21
        X12 = 2*N13 + N12 + N23
        X21 = 2*N31 + N21 + N32
        X22 = 2*N33 + N23 + N32
        phat = (N11+N12+N13+1./2*(N21+N22+N23)) / n
        qhat = (N11+N21+N31+1./2*(N12+N22+N32)) / n
        f11_guess = 1./4/n * (X11-X12-X21+X22)+1./2-(1-phat)*(1-qhat)
        last_guess = 1000
        while abs(last_guess-f11_guess) > tol:
            last_guess = f11_guess
            f11_guess = (X11 + N22*f11_guess*(1-phat-qhat+f11_guess)/(f11_guess*(1-phat-qhat+f11_guess)+(phat-f11_guess)*(qhat-f11_guess))) / 2/n
        if f11_guess < 0 or f11_guess > 1:
            f11_guess = np.nan
        Dhat = f11_guess - phat * qhat
        em_r2[i] = Dhat**2 / (phat*(1-phat)*qhat*(1-qhat))
    return em_r2

def hill_rs_root(Gs, n):
    em_r2 = np.zeros(len(Gs[0]))
    for i in range(len(Gs[0])):
        N11,N12,N13,N21,N22,N23,N31,N32,N33 = Gs[:,i]
        X11 = 2*N11 + N12 + N21
        X12 = 2*N13 + N12 + N23
        X21 = 2*N31 + N21 + N32
        X22 = 2*N33 + N23 + N32
        phat = (N11+N12+N13+1./2*(N21+N22+N23)) / n
        qhat = (N11+N21+N31+1./2*(N12+N22+N32)) / n
        coeffs = [-4*n, 
                  -2*n+N22+4*n*phat+4*n*qhat+2*X11, 
                  N22-N22*phat-N22*qhat-2*n*phat*qhat+X11-2*phat*X11-2*qhat*X11, 
                  phat*qhat*X11]
        f11_roots = np.roots(coeffs)
        try:
            f11_guess = f11_roots[np.where(f11_roots.imag == 0.0)[0][0]].real
        except IndexError:
            f11_guess = np.nan
        Dhat = f11_guess - phat * qhat
        em_r2[i] = Dhat**2 / (phat*(1-phat)*qhat*(1-qhat))
    return em_r2


freqs = [ ( 0.5, 0.5, 0.0 ),
          ( 0.4, 0.6, 0.1 ),
          ( 0.1, 0.2, 0.05 ),
          ( 0.5, 0.05, -0.02 ),
          ( 0.25, 0.01, 0.005 ),
          ( 0.1, 0.1, 0.09 ),
          ( 0.25, 0.25, 0.0 )] 

data = {}
s = 1000000

ns = [10,20,40,100,200,400]

for p,q,D in freqs:
    print(p,q,D)
    fAB = p*q+D
    fAb = p*(1-q)-D
    faB = (1-p)*q-D
    fab = (1-p)*(1-q)+D

    r2 = D**2/(p*(1-p)*q*(1-q))
    print(r2)

    (g1,g2,g3,g4,g5,g6,g7,g8,g9) = (fAB**2, 2*fAB*fAb, fAb**2, 2*fAB*faB, 2*fAB*fab+2*fAb*faB, 2*fAb*fab, faB**2, 2*faB*fab, fab**2)


    mean_r2s = {}
    var_r2s = {}
    
#    mean_r2s['RH'] = np.zeros(len(ns))
#    mean_r2s['AR'] = np.zeros(len(ns))
#    var_r2s['RH'] = np.zeros(len(ns))
#    var_r2s['AR'] = np.zeros(len(ns))
#    mean_r2s['B'] = np.zeros(len(ns))
#    var_r2s['B'] = np.zeros(len(ns))
#    mean_r2s['BS'] = np.zeros(len(ns))
#    var_r2s['BS'] = np.zeros(len(ns))
    mean_r2s['EM'] = np.zeros(len(ns))
    var_r2s['EM'] = np.zeros(len(ns))
    
    for ii,n in enumerate(ns):
        Gs = np.random.multinomial(n, (g1,g2,g3,g4,g5,g6,g7,g8,g9), size=s).transpose()
#        r2_AR = ragsdale_r2(Gs)
#        mean_r2s['AR'][ii] = np.mean(r2_AR[np.isnan(r2_AR) == False])
#        var_r2s['AR'][ii] = np.var(r2_AR[np.isnan(r2_AR) == False])
#        
#        rs_RH = rogers_r2(Gs)
#        mean_r2s['RH'][ii] = np.mean(rs_RH[np.isnan(rs_RH) == False]**2)
#        var_r2s['RH'][ii] = np.var(rs_RH[np.isnan(rs_RH) == False]**2)
#        
#        r2_b = burrows_r2(Gs, n)
#        mean_r2s['B'][ii] = np.mean(r2_b[np.isnan(r2_b)==False])
#        var_r2s['B'][ii] = np.var(r2_b[np.isnan(r2_b)==False])
#        
#        r2_bs = bulik_sullivan(Gs, n)
#        mean_r2s['BS'][ii] = np.mean(r2_bs[np.isnan(r2_bs)==False])
#        var_r2s['BS'][ii] = np.var(r2_bs[np.isnan(r2_bs)==False])
        
        rs_em = hill_rs(Gs, n)
        mean_r2s['EM'][ii] = np.mean(rs_em[np.isnan(rs_em)==False])
        var_r2s['EM'][ii] = np.var(rs_em[np.isnan(rs_em)==False])
        
        print(n, mean_r2s['EM'][ii])
    
#    data[(p,q,D)] = {'mean': mean_r2s, 'var': var_r2s}
    data[(p,q,D)]['mean']['EM'] = mean_r2s['EM']
    data[(p,q,D)]['var']['EM'] = var_r2s['EM']

pickle.dump(data, open('mean_var_r2_all.bp','wb+'))



### timing it

num_pairs = 10000

p,q,D = (0.5,0.5,0.0)
fAB = p*q+D
fAb = p*(1-q)-D
faB = (1-p)*q-D
fab = (1-p)*(1-q)+D
(g1,g2,g3,g4,g5,g6,g7,g8,g9) = (fAB**2, 2*fAB*fAb, fAb**2, 2*fAB*faB, 2*fAB*fab+2*fAb*faB, 2*fAb*fab, faB**2, 2*faB*fab, fab**2)

ns = (5,10,20,40,80,160,320) # num diploids

def ragsdale(G_counts):
    D2 = sgc.DD([Gs],[0,0])
    pi2 = sgc.pi2([Gs], [0,0,0,0])
    return D2/pi2

def ragsdale_tally(G_arrays):
    G_tally = np.zeros((9,len(G_arrays)))
    for i in range(len(G_arrays)):
        G_tally[:,i] = mold.Parsing.g_tally_counter(G_arrays[i][0],G_arrays[i][1])
    D2 = sgc.DD([G_tally],[0,0])
    pi2 = sgc.pi2([G_tally], [0,0,0,0])
    return D2/pi2

def rogers_huff(G_arrays):
    r2s = np.zeros(len(G_arrays))
    for i in range(len(r2s)):
        r2s[i] = np.corrcoef(G_arrays[i][0],G_arrays[i][1])[0,1]
    return r2s

import time

for n in ns:
    Gs = np.random.multinomial(n, (g1,g2,g3,g4,g5,g6,g7,g8,g9), size=num_pairs).transpose()
    G_arrays = []
    for i in range(num_pairs):
        gl = [2]*Gs[0,i] + [2]*Gs[1,i] + [2]*Gs[2,i] + [1]*Gs[3,i] + [1]*Gs[4,i] + [1]*Gs[5,i] +  [0]*Gs[6,i] + [0]*Gs[7,i] + [0]*Gs[8,i]
        gr = [2]*Gs[0,i] + [1]*Gs[1,i] + [0]*Gs[2,i] + [2]*Gs[3,i] + [1]*Gs[4,i] + [0]*Gs[5,i] +  [2]*Gs[6,i] + [1]*Gs[7,i] + [0]*Gs[8,i]
        G_arrays.append([np.array(gl), np.array(gr)])
    
    time1 = time.time()
    r2s_ragsdale = ragsdale(Gs)
    time2 = time.time()
    r2s_ragsdale_tally = ragsdale_tally(G_arrays)
    time3 = time.time()
    r2s_rogers = rogers_huff(G_arrays)
    time4 = time.time()
    
    print("sample size : ", n)
    print("ragsdale, pre-tallied : ", time2-time1)
    print("ragsdale, untallied   : ", time3-time2)
    print("rogers                : ", time4-time3)
    print("\t")


# for a block of snps

num_loci = 1000
ns = [5,10,20,40,80,160,320,640,1280,2560,5210] # num diploids

def rogers_block(G):
    n,L = np.shape(G)
    X = np.empty(L*(L-1)//2)
    c = 0
    for i in range(L)[:-1]:
        for j in range(i+1,L):
            X[c] = np.corrcoef(G[:,i],G[:,j])[0,1] ** 2
            c += 1
    return X

def ragsdale_block(G):
    Counts = mold.Parsing.count_genotypes(G.transpose())
    D2 = mold.Parsing.compute_D2(Counts)
    pi2 = mold.Parsing.compute_pi2(Counts)
    return (D2/pi2) ** 2

times_ragsdale = []
times_rogers = []

print("total number of pairs : ", num_loci*(num_loci-1)//2)
for n in ns:
    G = np.random.randint(3, size=(n, num_loci))
    time1 = time.time()
    r2s_rogers_block = rogers_block(G)
    time2 = time.time()
    r2s_ragsdale_block = ragsdale_block(G)
    time3 = time.time()
    print("sample size : ", n)
    print("ragsdale    : ", time3-time2)
    print("rogers      : ", time2-time1)
    print("")
    times_ragsdale.append(time3-time2)
    times_rogers.append(time2-time1)

import matplotlib.pylab as plt

plt.plot(ns, times_ragsdale, label='R/G')
plt.plot(ns, times_rogers, label='R/H')

plt.legend(frameon=False)

plt.xlabel('Diploid sample size')
plt.ylabel('Total time (s)')

plt.title('Block of {0} SNPs'.format(num_loci))
plt.tight_layout()

plt.savefig('speed_comparison.pdf')

plt.xscale('log')
plt.yscale('log')

plt.savefig('speed_comparison_log.pdf')
plt.show()
