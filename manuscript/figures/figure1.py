import matplotlib.pylab as plt
import matplotlib
import numpy as np
import pickle
import matplotlib.image as mpimg
from scipy import ndimage


# Set fontsize to 8
matplotlib.rc('font',**{#'family':'sans-serif',
                        #'sans-serif':['Helvetica'],
                        'style':'normal',
                        'size':8 })
# Set label tick sizes to 6
matplotlib.rc('xtick', labelsize=6)
matplotlib.rc('ytick', labelsize=6)

ns = np.array([10,20,40,100,200,400])
#data_r2 = pickle.load(open('data/mean_var_r2_burrows.bp','rb'))
data_r2 = pickle.load(open('data/mean_var_r2_all.bp','rb'))
data_D2 = pickle.load(open('data/mean_D2.bp','rb'))

#waples_correction = (0.0018+0.907/ns+4.44/ns**2) * (ns<30) + (1./ns+3.19/ns**2) * (ns>=30)


fig = plt.figure(1, figsize=(6.5, 2.9))
fig.clf()

p,q,D = (0.4, 0.6, 0.1)
D2s = data_D2[(p,q,D)]['mean']
r2s = data_r2[(p,q,D)]['mean']
Er2 = D**2 / (p*(1-p)*q*(1-q))

ax1 = plt.subplot2grid((2,7), (0,0), colspan=2)


ax1.plot(ns, D2s['Cov'], '--', label=r'$\widehat{D}^2$', color='k', linewidth=1)

ax1.plot(ns, D2s['AR'], '-', label=r'$\widehat{D^2}$', color='k', linewidth=1)

ax1.plot(ns, [D**2] * len(ns), ':', label='Truth', color='k', linewidth=1)

ax1.legend(frameon=False, fontsize=6, handlelength=3)
ax1.set_title(r'$p={0}$, $q={1}$, $D={2}$'.format(p,q,D), fontsize=7)
ax1.set_ylabel('$D^2$')

ax1.set_xlim([0,ns[-1]])



ax2 = plt.subplot2grid((2,7), (1,0), colspan=2)

ax2.plot(ns, r2s['RH'], '--', label=r'$r^2_{RH}$', color='black', linewidth=1)

ax2.plot(ns, r2s['AR'], '-', label=r'$r^2_\div$', color='black', linewidth=1)

## waples correction
#ax2.plot(ns, r2s['B'] - waples_correction, '-.', label=r'$r^2_{W}$', color='black', linewidth=1)
ax2.plot(ns, r2s['EM'], '-.', label=r'$r^2_{EM}$', color='black', linewidth=1)

ax2.plot(ns, [Er2] * len(ns), ':', color='gray', label='Truth', linewidth=1)


ax2.legend(frameon=False, fontsize=6, handlelength=3)
ax2.set_ylabel('$r^2$')

ax2.set_xlim([0,ns[-1]])

ax2.set_xlabel('Diploid sample size')


p,q,D = (0.5, 0.05, -0.02)
D2s = data_D2[(p,q,D)]['mean']
r2s = data_r2[(p,q,D)]['mean']
Er2 = D**2 / (p*(1-p)*q*(1-q))

ax3 = plt.subplot2grid((2,7), (0,2), colspan=2)


ax3.plot(ns, D2s['Cov'], 'k--', label='Squared Cov.', linewidth=1)

ax3.plot(ns, D2s['AR'], 'k-', label='Our estimator', linewidth=1)

ax3.plot(ns, [D**2] * len(ns), 'k:', label='Truth', linewidth=1)

#ax3.legend(frameon=False, fontsize=6)
ax3.set_title(r'$p={0}$, $q={1}$, $D={2}$'.format(p,q,D), fontsize=7)
#ax3.set_ylabel('$D^2$')
ax3.set_xlim([0,ns[-1]])

ax3.set_yticks([0.0005,0.001,0.0015])

ax4 = plt.subplot2grid((2,7), (1,2), colspan=2)


ax4.plot(ns, r2s['RH'], '--', label='Rogers-Huff', color='black', linewidth=1)

ax4.plot(ns, r2s['AR'], '-', label='Our estimator', color='black', linewidth=1)

## waples correction
#ax4.plot(ns, r2s['B'] - waples_correction, '-.', label=r'$r^2_{W}$', markersize=1, color='black', linewidth=1)
ax4.plot(ns, r2s['EM'], '-.', label=r'$r^2_{EM}$', color='black', linewidth=1)

ax4.plot(ns, [Er2] * len(ns), ':', label='Truth', linewidth=1, color='gray')

#ax4.legend(frameon=False, fontsize=6)
#ax4.set_ylabel('$r^2$')

ax4.set_xlim([0,ns[-1]])

ax4.set_xlabel('Diploid sample size')


ax5 = plt.subplot2grid((2,7), (0,4), rowspan=2, colspan=3)

img=mpimg.imread('./data/R2comp_bw.png')
rotated_img = ndimage.rotate(img, -45)
ax5.imshow(rotated_img)
ax5.axis('off')

(x0,x1) = ax5.get_xlim()
(y1,y0) = ax5.get_ylim()
plt.plot((x0, (x0+x1)/2), ((y0+y1)/2, y0), 'k-', linewidth=.25)
plt.plot((x1, (x0+x1)/2), ((y0+y1)/2, y0), 'k-', linewidth=.25)
plt.plot((x0, (x0+x1)/2), ((y0+y1)/2, y1), 'k-', linewidth=.25)
plt.plot((x1, (x0+x1)/2), ((y0+y1)/2, y1), 'k-', linewidth=.25)

pad = (x1-x0) / 20.

ax5.text( x0-pad*2/3 , (y0+y1)/2-pad*2/3, '0', va='center', ha='center', rotation=45, fontsize=6)
ax5.text( x0-pad*2/3 , (y0+y1)/2+pad*2/3, '0', va='center', ha='center', rotation=-45, fontsize=6)
ax5.text( (x0+x1)/2-pad*2/3 , y0-pad*2/3, '500', va='center', ha='center', rotation=45, fontsize=6)
ax5.text( (x0+x1)/2-pad*2/3 , y1+pad*2/3, '500', va='center', ha='center', rotation=-45, fontsize=6)

ax5.text( x0/2 + (x0+x1)/4 - pad, (y0+y1)/4 + y0/2 - pad, 'SNP', va='center', ha='center', rotation=45, fontsize=7)
ax5.text( x0/2 + (x0+x1)/4 - pad, (y0+y1)/4 + y1/2 + pad, 'SNP', va='center', ha='center', rotation=-45, fontsize=7)

ax5.text( 1/8 * x0 + 7/8 * x1, 7/8 * y0 + 1/8 * y1, 'Pairwise $r^2_\div$', va='center', ha='center')
ax5.text( 1/8 * x0 + 7/8 * x1, 1/8 * y0 + 7/8 * y1, 'Pairwise $r^2_{RH}$', va='center', ha='center')




#ax5.text(.5,.5,'Haploview, \n Ragsdale vs Rogers', va='center', ha ='center')

fig.text(.025,.96, 'A', va='center', ha='center', fontsize=9)
fig.text(.325,.96, 'B', va='center', ha='center', fontsize=9)
fig.text(.025,.49, 'C', va='center', ha='center', fontsize=9)
fig.text(.325,.49, 'D', va='center', ha='center', fontsize=9)
fig.text(.65,.8, 'E', va='center', ha='center', fontsize=9)

fig.tight_layout()
fig.subplots_adjust(hspace=0.25, wspace=1.0)

# insert the colorbar
ax_cb = fig.add_axes([0.97, .35, .02, .3])

img_cb=mpimg.imread('./data/colorbar.png')
rotated_colorbar = ndimage.rotate(img_cb, 90)
ax_cb.imshow(rotated_colorbar)
ax_cb.axis('off')

fig.text(.99, .35, '0', va='center', ha='center', fontsize=6)
fig.text(.99, .65, '1', va='center', ha='center', fontsize=6)

plt.savefig('figure1.pdf', dpi=300)
