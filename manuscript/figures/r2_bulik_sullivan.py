import matplotlib.pylab as plt
import matplotlib
import numpy as np
import pickle
import matplotlib.image as mpimg
from scipy import ndimage


# Set fontsize to 8
matplotlib.rc('font',**{'family':'sans-serif',
                        'sans-serif':['Helvetica'],
                        'style':'normal',
                        'size':8 })
# Set label tick sizes to 6
matplotlib.rc('xtick', labelsize=6)
matplotlib.rc('ytick', labelsize=6)

ns = np.array([10,20,40,100,200,400])
data_r2 = pickle.load(open('data/mean_var_r2_all.bp','rb'))
waples_correction = (0.0018+0.907/ns+4.44/ns**2) * (ns<30) + (1./ns+3.19/ns**2) * (ns>=30)


fig = plt.figure(1, figsize=(6.5,6))
fig.clf()

p,q,D = (0.4, 0.6, 0.1)
r2s = data_r2[(p,q,D)]['mean']
Er2 = D**2 / (p*(1-p)*q*(1-q))

ax1 = plt.subplot(3,2,1)

ax1.plot(ns, r2s['AR'], '-', label=r'$r^2_\div$', color='slateblue')

ax1.plot(ns, r2s['RH'], '--', label=r'$r^2_{RH}$', color='green')

ax1.plot(ns, r2s['BS'], label=r'$r^2_{BS}$', color='firebrick', dashes=[4, 2, 1, 2, 1, 2])

ax1.plot(ns, r2s['B'] - waples_correction, ':', label=r'$r^2_{W}$', color='darkorange')

ax1.plot(ns, r2s['EM'], '-.', label=r'$r^2_{EM}$', color='plum')

ax1.plot(ns, [Er2] * len(ns), 'k--', label='Truth', linewidth=1)

ax1.legend(frameon=False, fontsize=6, handlelength=4)
ax1.set_title(r'$p={0}$, $q={1}$, $D={2}$'.format(p,q,D), fontsize=7)
ax1.set_ylabel('$r^2$')

ax1.set_xlim([0,ns[-1]])
#ax1.set_xlabel('Diploid sample size')


p,q,D = (0.5, 0.05, -0.02)
r2s = data_r2[(p,q,D)]['mean']
Er2 = D**2 / (p*(1-p)*q*(1-q))

ax2 = plt.subplot(3,2,2)

ax2.plot(ns, r2s['AR'], '-', label=r'$r^2_\div$', color='slateblue')

ax2.plot(ns, r2s['RH'], '--', label=r'$r^2_{RH}$', color='green')

ax2.plot(ns, r2s['BS'], label=r'$r^2_{BS}$', color='firebrick', dashes=[4, 2, 1, 2, 1, 2])

ax2.plot(ns, r2s['B'] - waples_correction, ':', label=r'$r^2_{W}$', color='darkorange')

ax2.plot(ns, r2s['EM'], '-.', label=r'$r^2_{EM}$', color='plum')

ax2.plot(ns, [Er2] * len(ns), 'k--', label='Truth', linewidth=1)


ax2.set_title(r'$p={0}$, $q={1}$, $D={2}$'.format(p,q,D), fontsize=7)
#ax2.legend(frameon=False, fontsize=6)

ax2.set_xlim([0,ns[-1]])

#ax2.set_xlabel('Diploid sample size')


p,q,D = (0.1, 0.2, 0.05)
r2s = data_r2[(p,q,D)]['mean']
Er2 = D**2 / (p*(1-p)*q*(1-q))

ax3 = plt.subplot(3,2,3)

ax3.plot(ns, r2s['AR'], '-', label=r'$r^2_\div$', color='slateblue')

ax3.plot(ns, r2s['RH'], '--', label=r'$r^2_{RH}$', color='green')

ax3.plot(ns, r2s['BS'], label=r'$r^2_{BS}$', color='firebrick', dashes=[4, 2, 1, 2, 1, 2])

ax3.plot(ns, r2s['B'] - waples_correction, ':', label=r'$r^2_{W}$', color='darkorange')

ax3.plot(ns, r2s['EM'], '-.', label=r'$r^2_{EM}$', color='plum')

ax3.plot(ns, [Er2] * len(ns), 'k--', label='Truth', linewidth=1)

#ax3.legend(frameon=False, fontsize=6)
ax3.set_title(r'$p={0}$, $q={1}$, $D={2}$'.format(p,q,D), fontsize=7)
ax3.set_ylabel('$r^2$')

ax3.set_xlim([0,ns[-1]])


p,q,D = (0.25, 0.01, 0.005)
r2s = data_r2[(p,q,D)]['mean']
Er2 = D**2 / (p*(1-p)*q*(1-q))

ax4 = plt.subplot(3,2,4)

ax4.plot(ns, r2s['AR'], '-', label=r'$r^2_\div$', color='slateblue')

ax4.plot(ns, r2s['RH'], '--', label=r'$r^2_{RH}$', color='green')

ax4.plot(ns, r2s['BS'], label=r'$r^2_{BS}$', color='firebrick', dashes=[4, 2, 1, 2, 1, 2])

ax4.plot(ns, r2s['B'] - waples_correction, ':', label=r'$r^2_{W}$', color='darkorange')

ax4.plot(ns, r2s['EM'], '-.', label=r'$r^2_{EM}$', color='plum')

ax4.plot(ns, [Er2] * len(ns), 'k--', label='Truth', linewidth=1)


ax4.set_title(r'$p={0}$, $q={1}$, $D={2}$'.format(p,q,D), fontsize=7)
#ax4.legend(frameon=False, fontsize=6)

ax4.set_xlim([0,ns[-1]])




p,q,D = ( 0.1, 0.1, 0.09 )
r2s = data_r2[(p,q,D)]['mean']
Er2 = D**2 / (p*(1-p)*q*(1-q))

ax5 = plt.subplot(3,2,5)

ax5.plot(ns, r2s['AR'], '-', label=r'$r^2_\div$', color='slateblue')

ax5.plot(ns, r2s['RH'], '--', label=r'$r^2_{RH}$', color='green')

ax5.plot(ns, r2s['BS'], label=r'$r^2_{BS}$', color='firebrick', dashes=[4, 2, 1, 2, 1, 2])

ax5.plot(ns, r2s['B'] - waples_correction, ':', label=r'$r^2_{W}$', color='darkorange')

ax5.plot(ns, r2s['EM'], '-.', label=r'$r^2_{EM}$', color='plum')

ax5.plot(ns, [Er2] * len(ns), 'k--', label='Truth', linewidth=1)

#ax5.legend(frameon=False, fontsize=6)
ax5.set_title(r'$p={0}$, $q={1}$, $D={2}$'.format(p,q,D), fontsize=7)
ax5.set_ylabel('$r^2$')

ax5.set_xlim([0,ns[-1]])
ax5.set_xlabel('Diploid sample size')


p,q,D = ( 0.25, 0.25, 0.0 )
r2s = data_r2[(p,q,D)]['mean']
Er2 = D**2 / (p*(1-p)*q*(1-q))

ax6 = plt.subplot(3,2,6)

ax6.plot(ns, r2s['AR'], '-', label=r'$r^2_\div$', color='slateblue')

ax6.plot(ns, r2s['RH'], '--', label=r'$r^2_{RH}$', color='green')

ax6.plot(ns, r2s['BS'], label=r'$r^2_{BS}$', color='firebrick', dashes=[4, 2, 1, 2, 1, 2])

ax6.plot(ns, r2s['B'] - waples_correction, ':', label=r'$r^2_{W}$', color='darkorange')

ax6.plot(ns, r2s['EM'], '-.', label=r'$r^2_{EM}$', color='plum')

ax6.plot(ns, [Er2] * len(ns), 'k--', label='Truth', linewidth=1)


ax6.set_title(r'$p={0}$, $q={1}$, $D={2}$'.format(p,q,D), fontsize=7)
#ax6.legend(frameon=False, fontsize=6)

ax6.set_xlim([0,ns[-1]])

ax6.set_xlabel('Diploid sample size')


fig.tight_layout()
plt.savefig('bulik_sullivan.pdf', dpi=300)
