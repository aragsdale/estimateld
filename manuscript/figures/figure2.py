import matplotlib.pylab as plt
import matplotlib
import numpy as np
import pickle
import pickle, numpy as np
import matplotlib.pylab as plt
from matplotlib.ticker import FuncFormatter


# Set fontsize to 10
matplotlib.rc('font',**{#'family':'sans-serif',
                        #'sans-serif':['Helvetica'],
                        'style':'normal',
                        'size':8 })
# Set label tick sizes to 8
matplotlib.rc('xtick', labelsize=6)
matplotlib.rc('ytick', labelsize=6)


msprime_r2 = pickle.load(open('./data/simulated_r2s.bp','rb'))
r = 2e-8
bins = np.linspace(0,5e-3,21)


pops = ['YRI', 'CDX', 'CEU', 'MXL', 'PUR']

r_bins = np.linspace(0,1e-2,21)
r_bins = list(zip(r_bins[:-1],r_bins[1:]))

data = pickle.load(open('data/r2s_1kg.bp','rb'))
r2arr = data['r2arr']
s2arr = data['s2arr']
RHarr = data['RHarr']

colors = {'YRI':'violet', 'CDX':'slateblue', 'MXL':'firebrick', 'CEU':'olivedrab', 'PUR':'darkorange'}
markers = {'YRI':'v', 'CDX':'o', 'MXL':'s', 'CEU':'^', 'PUR':'D'}

fig = plt.figure(2, figsize=(6.5, 4))
fig.clf()



ax1 = plt.subplot(2,2,1)

ax1.plot(100*(bins[:-1]+bins[1:])/2, msprime_r2['RH_genotypes'], 's', markerfacecolor='none', markersize=3, label='$\hat{r}^2$ unphased (RH)', color='k')#color='firebrick')
ax1.plot(100*(bins[:-1]+bins[1:])/2, msprime_r2['RH_haplotypes'], 'D', markerfacecolor='none', markersize=3, label='$\hat{r}^2$ phased', color='k')#color='forestgreen')
ax1.plot(100*(bins[:-1]+bins[1:])/2, msprime_r2['AR_genotypes'], 'x', markersize=3, label='$r^2_\div$ unphased', color='k')#color='darkorange')
ax1.plot(100*(bins[:-1]+bins[1:])/2, msprime_r2['AR_haplotypes'], 'o', markerfacecolor='none', markersize=5, label='$r^2_\div$ phased', color='k')#color='steelblue')


ax1.set_yscale('log')
ax1.set_xscale('log')

ax1.set_ylabel('$r^2$')
ax1.set_xlabel('cM')

ax1.set_ylim(top=.1)

ax1.set_xticks([0.01,0.1,0.5])

for axis in [ax1.xaxis]:
    formatter = FuncFormatter(lambda y, _: '{:.16g}'.format(y))
    axis.set_major_formatter(formatter)

ax1.legend(frameon=False, fontsize=6)


ax2 = plt.subplot(2,2,2)

for pop in pops:
    ax2.plot(100*np.mean(r_bins,axis=1), RHarr[pop], '.-', marker=markers[pop], linewidth=.5, markersize=2, label=pop, color='k')#color=colors[pop], 

ax2.set_yscale('log')
ax2.set_xscale('log')

ax2.set_ylabel('$r^2_{RH}$')
ax2.set_xlabel('cM')

ax2.set_ylim(top=1e-1)

for axis in [ax2.xaxis]:
    formatter = FuncFormatter(lambda y, _: '{:.16g}'.format(y))
    axis.set_major_formatter(formatter)

ax2.legend(frameon=False, loc='lower left', fontsize=6)


ax3 = plt.subplot(2,2,3)

for pop in pops:
    ax3.plot(100*np.mean(r_bins,axis=1), r2arr[pop], '.-', marker=markers[pop], linewidth=.5, markersize=2, label=pop, color='k')#color=colors[pop],

ax3.set_yscale('log')
ax3.set_xscale('log')

ax3.set_ylabel('$r^2_\div$')
ax3.set_xlabel('cM')

for axis in [ax3.xaxis]:
    formatter = FuncFormatter(lambda y, _: '{:.16g}'.format(y))
    axis.set_major_formatter(formatter)

ax3.legend(frameon=False, loc='lower left', fontsize=6)

#ax3.set_ylim(top=1e-1, bottom=2e-4)


ax4 = plt.subplot(2,2,4)

for pop in pops:
    ax4.plot(100*np.mean(r_bins,axis=1), s2arr[pop], '-', marker=markers[pop], linewidth=.5, markersize=2, label=pop, color='k')#color=colors[pop],)

ax4.set_yscale('log')
ax4.set_xscale('log')

ax4.set_ylabel('$\sigma_D^2$')
ax4.set_xlabel('cM')

for axis in [ax4.xaxis]:
    formatter = FuncFormatter(lambda y, _: '{:.16g}'.format(y))
    axis.set_major_formatter(formatter)

ax4.legend(frameon=False, loc='lower left', fontsize=6)


fig.text(.025,.96, 'A', va='center', ha='center', fontsize=9)
fig.text(.525,.96, 'B', va='center', ha='center', fontsize=9)
fig.text(.025,.475, 'C', va='center', ha='center', fontsize=9)
fig.text(.525,.475, 'D', va='center', ha='center', fontsize=9)

fig.tight_layout()
plt.savefig('figure2.pdf'.format(pops[0]), dpi=300)
